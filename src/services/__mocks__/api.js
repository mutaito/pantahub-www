/* eslint-disable camelcase */
/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as DeviceMocks from '../../mocks/devices'
import * as SummaryMocks from '../../mocks/device_summary'
import * as StepsMocks from '../../mocks/trails_steps'
import * as ObjectMocks from '../../mocks/objects'

export const login = async (username, password) =>
  null

export const register = async (nick, email, password) =>
  null

export const verifyAccount = async (id, challenge) =>
  null

export const getUserData = async (token) =>
  null

export const getDashData = async token =>
  null

export const getDevsData = async token =>
  null

export const getDeviceData = async (token, id) => ({
  ok: true,
  status: 200,
  json: DeviceMocks.device
})

export const getDeviceSummary = async (token, id) => ({
  ok: true,
  status: 200,
  json: SummaryMocks.summary
})

export const getDeviceSteps = async (token, id, status) => ({
  ok: true,
  status: 200,
  json: StepsMocks.steps
})

export const getDeviceObjects = async (token, id, rev) => ({
  ok: true,
  status: 200,
  json: ObjectMocks.objects
})

export const getDeviceLogs = async (
  token,
  id,
  after = '1970-01-01T00:00:00.0Z',
  page = 2000,
  sort = '-time-created'
) =>
  null

export const deleteDevice = async (token, id) =>
  null

export const publishDevice = async (token, id) =>
  null

export const unPublishDevice = async (token, id) =>
  null

export const claimDevice = async (token, id, secret) =>
  null

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  null

export const fetchFile = async (token, url) =>
  null

export const patchDevice = async (token, id, payload) =>
  null

export const postOauth2Authorize = async (service, scopes, redirect_uri, state, token, response_type) =>
  null
