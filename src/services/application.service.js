/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
  _getJSON,
  _postJSON,
  _putJSON,
  _delete,
  API_URL
} from './api.service'

const APPS_URL = `${API_URL}/apps/`

export const getScopes = async (token, id) => {
  const URL = id ? `${APPS_URL}scopes?serviceID=${encodeURIComponent(id)}` : `${APPS_URL}scopes`
  return _getJSON(URL, token)
}

export const getApps = async (token) =>
  _getJSON(APPS_URL, token)

export const createApps = async (token, payload) =>
  _postJSON(APPS_URL, token, payload)

export const updateApp = async (token, id, payload) =>
  _putJSON(`${APPS_URL}${id}`, token, payload)

export const getApp = async (token, id) =>
  _getJSON(`${APPS_URL}${id}`, token)

export const deleteApp = async (token, id) =>
  _delete(`${APPS_URL}${id}`, token)
