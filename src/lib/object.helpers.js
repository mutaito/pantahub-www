/**
 * resolvePath for objects
 * @param {*} obj
 * @param {string} path
 * @param {*} defaultReturn
 * @returns *
 */
export function resolvePath (obj, path, defaultReturn) {
  return path.split('.').reduce(function (prev, curr) {
    return prev && prev.hasOwnProperty(curr) ? prev[curr] : defaultReturn
  }, obj)
}

export function flatObject (input) {
  return Object.keys(input).reduce((prev, curr) => flat(prev, curr, input[curr]), {})
}

function flat (res, key, val, pre = '') {
  const prefix = [pre, key].filter(v => v).join('.')
  return typeof val === 'object'
    ? Object.keys(val).reduce((prev, curr) => flat(prev, curr, val[curr], prefix), res)
    : Object.assign(res, { [prefix]: val })
}
