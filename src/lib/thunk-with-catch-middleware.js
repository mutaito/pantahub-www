/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { notAuthorized, isForbidden } from '../lib/api.helpers'
import { logout } from '../store/auth/actions'
import { setError } from '../store/general-errors/actions'
import { push } from 'connected-react-router'

function processError (dispatch, getState) {
  return (error) => {
    if (notAuthorized(error)) {
      return dispatch(logout())
    }
    if (isForbidden(error)) {
      dispatch(push(`/u/${getState().auth.username}`))
      return dispatch(setError(error.status, error))
    }
  }
}

export const thunkWitAuthMiddleware = ({ dispatch, getState }) => next => action => {
  if (typeof action === 'function') {
    const result = action(dispatch, getState)
    if (typeof result.catch === 'function') {
      result.catch(processError(dispatch, getState))
    }
    return result
  }

  return next(action)
}
