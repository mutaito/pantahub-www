/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/* global localStorage */

import merge from 'lodash.merge'

import * as Types from './types'

import { tokenKey } from '../../lib/auth'

import jwtDecode from 'jwt-decode'

const authInfo = localStorage.getItem(tokenKey)
const [token, username] = (authInfo || '').split('|')
const decodeJwt = (jwt = '') => {
  try {
    return jwtDecode(jwt)
  } catch (e) {
    return {}
  }
}

const authInitialState = {
  username: username || '',
  password: '',
  email: '',
  token: token || null,
  exp: null,
  id: '',
  nick: '',
  orig_iat: null,
  prn: '',
  roles: '',
  type: '',
  gettingToken: false,
  getTokenError: null,
  signup: {
    loading: false,
    response: null,
    error: null
  },
  verification: {
    loading: false,
    response: null,
    error: null
  },
  setPassword: {
    loading: false,
    error: null,
    token: null
  },
  requestPassword: {
    done: false,
    loading: false,
    error: null
  },
  ...decodeJwt(token)
}

export const authReducer = (state = authInitialState, action) => {
  switch (action.type) {
    case Types.AUTH_SET_USER_DATA:
      return {
        ...state,
        ...action.payload
      }

    case Types.AUTH_SET_USERNAME:
      return Object.assign({}, state, {
        username: action.username
      })
    case Types.AUTH_SET_EMAIL:
      return Object.assign({}, state, {
        email: action.email
      })
    case Types.AUTH_SET_PASSWORD:
      return Object.assign({}, state, {
        password: action.password
      })
    case Types.AUTH_GET_TOKEN_INPROGR:
      return Object.assign({}, state, {
        loading: true,
        password: ''
      })
    case Types.AUTH_GET_TOKEN_SUCCESS:
      return Object.assign({}, state, {
        token: action.token,
        getTokenError: null,
        loading: false,
        ...decodeJwt(action.token)
      })
    case Types.AUTH_GET_TOKEN_FAILURE:
      return Object.assign({}, state, {
        getTokenError: action.error,
        loading: false
      })
    case Types.AUTH_REMOVE_TOKEN:
      return Object.assign({}, authInitialState, {
        token: null
      })
    case Types.AUTH_SIGNUP_INPROGR:
      return merge({}, state, {
        signup: {
          loading: true
        },
        password: ''
      })
    case Types.AUTH_SIGNUP_SUCCESS:
      return merge({}, state, {
        signup: {
          error: null,
          loading: false,
          response: action.response
        }
      })
    case Types.AUTH_SIGNUP_FAILURE:
      return merge({}, state, {
        signup: {
          loading: false,
          error: action.error
        }
      })

    case Types.AUTH_VERIFY_INPROGR:
      return merge({}, state, {
        verification: {
          loading: true
        }
      })
    case Types.AUTH_VERIFY_SUCCESS:
      return merge({}, state, {
        verification: {
          loading: false,
          response: action.response
        }
      })
    case Types.AUTH_VERIFY_FAILURE:
      return merge({}, state, {
        verification: {
          loading: false,
          error: action.error
        }
      })
    case Types.AUTH_REQUEST_PASSWORD:
      return state
    case Types.AUTH_REQUEST_PASSWORD_PROGRESS:
      return merge({}, state, {
        requestPassword: {
          loading: true,
          error: null
        }
      })
    case Types.AUTH_REQUEST_PASSWORD_SUCCESS:
      return merge({}, state, {
        requestPassword: {
          loading: false,
          done: true
        }
      })
    case Types.AUTH_REQUEST_PASSWORD_FAILURE:
      return merge({}, state, {
        requestPassword: {
          loading: false,
          error: action.error
        }
      })
    case Types.AUTH_SET_NEW_PASSWORD:
      return state
    case Types.AUTH_SET_NEW_PASSWORD_PROGRESS:
      return merge({}, state, {
        setPassword: {
          loading: true,
          error: null
        }
      })
    case Types.AUTH_SET_NEW_PASSWORD_SUCCESS:
      return merge({}, state, {
        setPassword: {
          loading: false,
          response: action.response
        }
      })
    case Types.AUTH_SET_NEW_PASSWORD_FAILURE:
      return merge({}, state, {
        setPassword: {
          loading: false,
          error: action.error
        }
      })
    case Types.AUTH_SET_EMAIL_TOKEN:
      return merge({}, state, {
        setPassword: {
          token: action.token
        }
      })
    default:
      return state
  }
}
