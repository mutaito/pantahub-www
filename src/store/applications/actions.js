/*
 * Copyright (c) 2017 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'

import * as Service from '../../services/application.service'
import { processService } from '../../services/api.service'
import { buildBasicActions } from '../../lib/redux.helpers'
import { catchError } from '../general-errors/actions'
import { push } from 'connected-react-router'
import { thridPartyApps } from '../../router/routes'

const createActions = buildBasicActions(Types, 'APP_CREATE')
const getAllActions = buildBasicActions(Types, 'APP_GET_ALL')
const getActions = buildBasicActions(Types, 'APP_GET')
const updateActions = buildBasicActions(Types, 'APP_UPDATE')
const deleteActions = buildBasicActions(Types, 'APP_DELETE')
const scopesActions = buildBasicActions(Types, 'APP_GET_SCOPES')

export const currentAppClean = () => ({
  type: Types.APP_CURRENT_CLEAN
})

export const toggleSorting = (field) => ({
  type: Types.APP_SET_SORT,
  field
})

export const getScopes = (appId) => async (dispatch, getState) => {
  dispatch(scopesActions.inProgress())
  const state = getState()

  return processService(
    Service.getScopes.bind(null, state.auth.token, appId),
    (resp) => dispatch(scopesActions.success(resp)),
    (error) => dispatch(catchError(error, scopesActions.failure))
  )
}

export const createApp = (payload) => async (dispatch, getState) => {
  dispatch(createActions.inProgress())
  const state = getState()

  return processService(
    Service.createApps.bind(null, state.auth.token, payload),
    (resp) => {
      dispatch(currentAppClean())
      dispatch(push(`/u/username${thridPartyApps}/${resp.id}`))
    },
    (error) => dispatch(catchError(error, createActions.failure))
  )
}

export const getApps = () => async (dispatch, getState) => {
  dispatch(getAllActions.inProgress())
  const state = getState()

  return processService(
    Service.getApps.bind(null, state.auth.token),
    (resp) => {
      dispatch(currentAppClean())
      dispatch(getAllActions.success(resp))
    },
    (error) => dispatch(catchError(error, getAllActions.failure))
  )
}

export const getApp = (id) => async (dispatch, getState) => {
  dispatch(getActions.inProgress())
  const state = getState()

  return processService(
    Service.getApp.bind(null, state.auth.token, id),
    (resp) => dispatch(getActions.success(resp)),
    (error) => dispatch(catchError(error, getActions.failure))
  )
}

export const updateApp = (payload, id) => async (dispatch, getState) => {
  if (!id) {
    dispatch(createApp(payload))
  }

  dispatch(updateActions.inProgress())
  const state = getState()

  return processService(
    Service.updateApp.bind(null, state.auth.token, id, payload),
    (resp) => {
      dispatch(currentAppClean())
      dispatch(push(`/u/username${thridPartyApps}/${resp.id}`))
    },
    (error) => dispatch(catchError(error, updateActions.failure))
  )
}

export const deleteApp = (id) => async (dispatch, getState) => {
  dispatch(deleteActions.inProgress())
  const state = getState()

  return processService(
    Service.deleteApp.bind(null, state.auth.token, id),
    (resp) => dispatch(deleteActions.success(resp)),
    (error) => dispatch(catchError(error, deleteActions.failure))
  )
}

export const deleteAppAndReload = (id) => async (dispatch, getState) => {
  dispatch(deleteActions.inProgress())
  const state = getState()

  return processService(
    Service.deleteApp.bind(null, state.auth.token, id),
    (_) => dispatch(getApps()),
    (error) => dispatch(catchError(error, deleteActions.failure))
  )
}
