/*
 * Copyright (c) 2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component, PureComponent } from 'react'
import ReactGA from 'react-ga'
import { resolvePath } from '../lib/object.helpers'
import { connect } from 'react-redux'

ReactGA.initialize(process.env.REACT_APP_GA_KEY, {
  appName: process.env.REACT_APP_TITLE
})

const defaultOptions = {}
const ANONIMOUS = 'anonymous'

const trackPage = (page, options = {}) => {
  ReactGA.set({
    page,
    ...Object.assign({}, defaultOptions, options)
  })
  ReactGA.pageview(page)
}

let currentPage = ''

export const googleAnalytics = store => next => action => {
  if (action.type === '@@router/LOCATION_CHANGE') {
    const nextPage = `${action.payload.location.pathname}${action.payload.location.search}`
    ReactGA.set({ userId: resolvePath(store.getState(), 'auth.prn', ANONIMOUS) })

    if (currentPage !== nextPage) {
      const options = {
        nick: resolvePath(store.getState(), 'auth.nick', ANONIMOUS),
        userId: resolvePath(store.getState(), 'auth.prn', ANONIMOUS)
      }
      currentPage = nextPage
      trackPage(nextPage, options)
    }
  }

  return next(action)
}

export const withOwnerTracker = (WrappedComponent, page, options = {}) => {
  const trackPage = (moreOptions = {}) => {
    ReactGA.set({
      page,
      ...Object.assign({}, defaultOptions, options, moreOptions)
    })
    ReactGA.pageview(page)
  }

  // eslint-disable-next-line
  const HOC = class extends Component {
    componentDidMount () {
      const options = {
        nick: resolvePath(this.props, 'auth.nick', ANONIMOUS),
        userId: resolvePath(this.props, 'auth.prn', ANONIMOUS)
      }
      trackPage(options)
    }

    componentDidUpdate (prevProps) {
      const currentPage =
        prevProps.location.pathname + prevProps.location.search
      const nextPage =
        this.props.location.pathname + this.props.location.search
      const options = {
        nick: resolvePath(this.props, 'auth.nick', ANONIMOUS),
        userId: resolvePath(this.props, 'auth.prn', ANONIMOUS)
      }

      if (currentPage !== nextPage) {
        trackPage(options)
      }
    }

    render () {
      return <WrappedComponent {...this.props} />
    }
  }

  return connect(
    (state) => ({ auth: state.auth })
  )(HOC)
}

export const withAuthenticatedEvent = (WrappedComponent, getPageName = () => '', options = { category: 'Navigation' }) => {
  const trackPage = (isLoggedIn = false) => {
    ReactGA.event({ ...options, action: getPageName(isLoggedIn) })
  }

  // eslint-disable-next-line
  const HOC = class extends PureComponent {
    componentDidMount () {
      trackPage(this.props.token && this.props.token !== '')
    }

    componentDidUpdate () {
      trackPage(this.props.token && this.props.token !== '')
    }

    render () {
      return <WrappedComponent {...this.props} />
    }
  }

  return connect(
    (state) => ({ token: state.auth.token })
  )(HOC)
}
