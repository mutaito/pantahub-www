import React, { useEffect, useState } from 'react'
import { resolvePath } from '../../../lib/object.helpers'
import Tile, { EMPTY_SOURCE } from '../Tile/Tile'

import './tiles.scss'

const DEFAULT_STATE_MAPPER = [
  '/src.json',
  '/run.json'
]

function getStateKeys (state = {}, types = DEFAULT_STATE_MAPPER) {
  const mappedStateByService = Object.keys(state)
    .reduce((acc, key) => {
      if (types.some(t => key.indexOf(t) >= 0) && key.indexOf('bsp') < 0) {
        const cleanKey = types.reduce((acc, t) => acc.replace(t, ''), key)
        acc[cleanKey] = {
          ...EMPTY_SOURCE,
          ...state[key],
          title: cleanKey
        }
      }
      return acc
    }, {})
  return Object.keys(mappedStateByService).reduce((acc, key) => [...acc, mappedStateByService[key]], [])
}

export default function Tiles (props) {
  const rawState = resolvePath(
    props,
    'device.history.currentStep.rawState',
    {}
  )
  const [sources, setSources] = useState([])

  useEffect(() => {
    setSources(getStateKeys(rawState))
  }, [rawState])
  return (
    <div className='device-tab__inner device-tiles'>
      <div className='device-tiles__inner pt-30 pb-30 pr-20 pl-20'>
        <div className="row row-cols-1 row-cols-md-3">
          {sources.map((s, index) => (
            <div className="col mb-4" key={s.docker_digest || index} >
              <Tile source={s} expansable={s.args && Object.keys(s.args).length > 0} />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
