/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import Form from 'react-bootstrap/Form'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'

/*
Props expected:
title: Title for button and list of options
filter: Object containing options in format:  {optionA: <checked?>, optionB: <checked?>, optionC: <checked?> }
onChangeFilterFieldOption: On change callback
field: Field id for callback
id: Field id for DOM
minOptions: Minimun of options to show the Component
*/
class SelectMultiple extends Component {
  state = { checkAll: true }

  onChangeCheckAll = () => {
    const { onChangeFilterFieldOption, field } = this.props
    const newState = { checkAll: !this.state.checkAll }
    this.setState(newState)
    onChangeFilterFieldOption(field, null, newState.checkAll)
  }

  onChangeFilterField = (option, value) => {
    const { onChangeFilterFieldOption, field } = this.props
    onChangeFilterFieldOption(field, option)
  }

  render () {
    const { title, filter, id, minOptions } = this.props

    const options = Object.entries(filter)

    const checkedCount = options.filter(([k, v]) => v).length
    const btnClass = 'mdi ' +
      (checkedCount === options.length
        ? 'mdi-checkbox-marked-outline'
        : (checkedCount === 0 ? 'mdi-checkbox-blank-outline' : 'mdi-checkbox-intermediate')
      )

    return (options.length >= (minOptions !== undefined ? minOptions : 1)
      ? (<OverlayTrigger
        placement="right"
        trigger="click"
        rootClose
        overlay={
          <Popover id={`select-multiple-${id}`}>
            <Popover.Title as="h3">{title}</Popover.Title>
            <Popover.Content>
              <Form>
                <Form.Check
                  id={`select-multiple-${id}-all`}
                  type="checkbox"
                  label={`All ${title}`}
                  checked={this.state.checkAll}
                  onChange={evt => {
                    this.onChangeCheckAll()
                  }}
                />
                {options.map(([k, v]) => (
                  <Form.Check
                    key={k}
                    id={`select-multiple-${id}-${k}`}
                    type="checkbox"
                    label={k}
                    checked={v}
                    onChange={evt => {
                      this.onChangeFilterField(k, evt.target.value)
                    }}
                  />
                ))}
              </Form>
            </Popover.Content>
          </Popover>}
      >
        <button
          className="mb-2 mr-sm-2 btn btn-sm btn-light"
          title={title}
          type="button"
        >{title}
          <span className="mb-2 ml-sm-2">
            <i className={btnClass} aria-hidden="true" />
          </span>
        </button>
      </OverlayTrigger>)
      : null)
  }
}

export default SelectMultiple
