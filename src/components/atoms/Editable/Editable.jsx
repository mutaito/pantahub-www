import React, { PureComponent } from 'react'

import isFunction from 'lodash.isfunction'

function placeCaretAtEnd (el) {
  el.focus()
  if (
    typeof window.getSelection !== 'undefined' &&
    typeof document.createRange !== 'undefined'
  ) {
    var range = document.createRange()
    range.selectNodeContents(el)
    range.collapse(false)
    var sel = window.getSelection()
    sel.removeAllRanges()
    sel.addRange(range)
  } else if (typeof document.body.createTextRange !== 'undefined') {
    var textRange = document.body.createTextRange()
    textRange.moveToElementText(el)
    textRange.collapse(false)
    textRange.select()
  }
}

function slugify (str, separator = '-') {
  return str
    .replace(/[^a-z0-9 +_-]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes
    .replace(/_+/g, '_') // collapse dashes
    .replace(/^-+/, '') // trim - from start of text
    .replace(/-+$/, '') // trim - from end of text
    .replace(/-/g, separator)
}

class EditableElement extends PureComponent {
  constructor (props) {
    super(props)
    this.input = React.createRef()
  }

  state = { editing: false, dirty: false, saving: false };

  componentDidMount () {
    const { value, saving } = this.props
    if (value === this.input.current.innerText && saving !== 'inprogr') { this.setState({ dirty: false }) }
  }

  componentDidUpdate () {
    const { value } = this.props
    const { editing, dirty } = this.state
    if (!editing && !dirty) this.input.current.innerText = value
  }

  startEditing = () => {
    const { editigHandler } = this.props
    this.setState({ editing: true })
    if (editigHandler) editigHandler(true)
    setTimeout(() => placeCaretAtEnd(this.input.current), 350)
  }

  cancelEditing = () => {
    const { value } = this.props
    this.input.current.classList.remove('dirty')
    this.input.current.innerText = value
    this.finishEditing()
  }

  finishEditing = () => {
    const { editigHandler } = this.props
    const { editing } = this.state
    if (editing) this.setState({ editing: false })
    if (isFunction(editigHandler)) editigHandler(false)
  }

  updateValue = (str) => {
    str = str.trim().toLowerCase().replace(/(\r\n|\n|\r)/gm, '').replace(/<br>/g, '')
    return this.props.slugify ? slugify(str) : str
  }

  onInput = () => {
    const { onChange } = this.props
    const editedValue = (this.input.current || {}).innerText || ''
    this.setState({ dirty: true })
    if (isFunction(onChange)) onChange(this.updateValue(editedValue))
  }

  onEnter = (event) => {
    switch (event.key) {
      case 'Enter':
        return this.state.editing && this.save()
      case 'Escape':
        return this.state.editing && this.cancelEditing()
      default:
    }
  }

  save = () => {
    const { saveHandler, value } = this.props
    const editedValue = (this.input.current || {}).innerText
    if (value !== editedValue) saveHandler(this.updateValue(editedValue))
    this.finishEditing()
  }

  onEditHandler = () => {
    const { editing } = this.state
    if (editing) this.save()
    else this.startEditing()
  }

  textValue = () => {
    const {
      value,
      truncate = false,
      size = 30
    } = this.props
    const { editing } = this.state
    return truncate && !editing && value.length > size
      ? value.substring(0, size) + ' ...'
      : value + ' '
  }

  render () {
    const {
      el,
      name,
      saving,
      className,
      editableClassName
    } = this.props

    const TagElement = el
    const saveInProgr = saving === 'inprogr'

    const { editing } = this.state
    const iconMdi = editing
      ? 'content-save'
      : saveInProgr
        ? 'progress-upload'
        : 'pencil'

    return (
      <React.Fragment>
        <TagElement
          name={name}
          className={`editable ${className || ''}`}
        >
          <span
            ref={this.input}
            className={`editable-inner ${editableClassName || ''}`}
            onInput={this.onInput}
            onKeyDown={this.onEnter}
            contentEditable={!saveInProgr && editing}
          >
            {this.textValue()}
          </span>
        </TagElement>
        <i
          className={`mdi mdi-${iconMdi}`}
          title={`${editing ? 'Save' : 'Edit'}`}
          onClick={this.onEditHandler}
        />
        {editing ? (
          <i
            className="mdi mdi-cancel"
            title="Cancel"
            onClick={this.cancelEditing}
          />
        ) : null}
      </React.Fragment>
    )
  }
}

export default EditableElement
