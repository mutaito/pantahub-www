/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  watchForNetworkDevices,
  unwatchForNetworkDevices,
  toggleManual
} from '../../../store/claims/actions'
import UnClaimedDevice from '../../molecules/UnClaimedDevice/UnClaimedDevice'
import Loading from '../../atoms/Loading/Loading'

import './claimdevicecordova.scss'
import { resolvePath } from '../../../lib/object.helpers'

class ClaimDevicesList extends Component {
  componentDidMount () {
    this.props.watch()
  }

  componentWillUnmount () {
    this.props.unwatch()
  }

  render () {
    const devices = Object.keys(this.props.devices).reduce((acc, key) => {
      if (resolvePath(this.props, `devices.${key}.secret`, null)) {
        acc.push(this.props.devices[key])
      }
      return acc
    }, [])
    return (
      <React.Fragment>
        <p>Scanning for devices in your local network...</p>
        {devices.length <= 0 && (
          <Loading/>
        )}
        {devices.length > 0 && (
          <section className="list">
            <header className="list-header">
              <p>
                We have found <b>{devices.length}</b> pantavisor {devices.length > 0 ? `devices` : `device`} in your network
              </p>
            </header>
            {devices.map((device) => (
              <UnClaimedDevice key={device.id} device={device}/>
            ))}
          </section>
        )}
      </React.Fragment>
    )
  }
}

function ClaimDeviceCordova (props) {
  return (
    <section className="local-claim">
      {!props.manual && (
        <ClaimDevicesList
          devices={props.devices}
          watch={props.watchForNetworkDevices}
          unwatch={props.unwatchForNetworkDevices}
        />
      )}
      <section className="actions">
        <button
          className="btn btn-link btn-small btn-block"
          onClick={props.toggleManual}
        >
          {props.manual ? 'Scan my network for devices' : 'Claim device manually'}
        </button>
      </section>
    </section>
  )
}

export default connect(
  state => state.claims,
  {
    watchForNetworkDevices,
    unwatchForNetworkDevices,
    toggleManual
  }
)(ClaimDeviceCordova)
