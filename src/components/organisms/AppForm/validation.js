import ValidaJs from 'valida-js'
import get from 'lodash.get'

ValidaJs.validators['in'] = (stateMap, type, name, compareWithValue = [], defaultValue = '') => {
  return (state) => {
    const value = get(state, stateMap, defaultValue)
    const isValid = compareWithValue.some(c => c === value)
    return ValidaJs.factoryValidationObj(isValid, type, name, `${name} should be in [${compareWithValue.join(', ')}] and got ${value || 'empty'}`)
  }
}

const rules = ValidaJs.rulesCreator(ValidaJs.validators, [
  {
    name: 'type',
    type: 'required',
    stateMap: 'type'
  },
  {
    name: 'type',
    type: 'in',
    stateMap: 'type',
    compareWith: ['public', 'confidential']
  },
  {
    name: 'nick',
    type: 'required',
    stateMap: 'nick'
  },
  {
    name: 'scopes',
    type: 'minLength',
    stateMap: 'scopes',
    compareWith: 1
  },
  {
    name: 'redirect_uris',
    type: 'minLength',
    stateMap: 'redirect_uris',
    compareWith: 1
  }
])

export const validate = (state) => ValidaJs.validate(rules, state)
