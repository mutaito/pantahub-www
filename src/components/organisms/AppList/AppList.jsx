/*
 * Copyright (c) 2017-2019 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { STATES } from '../../../store/applications/reducers'
import Loading from '../../atoms/Loading/Loading'
import ConfirmButton from '../../atoms/ConfirmButton/ConfirmButton'
import { thridPartyAppsEdit, thridPartyApps } from '../../../router/routes'
import { Link } from 'react-router-dom'

const sortableFields = [
  ['id', 'App ID'],
  ['nick', 'Name'],
  ['prn', 'PRN']
]
const sortFieldActive = (sortField, field) => (sortField === field ? 'active' : null)

const sortIconForField = (sortField, sortDirection, field) =>
  sortField === field
    ? sortDirection === -1
      ? 'chevron-down'
      : 'chevron-up'
    : 'unfold-more-horizontal'

const AppRow = ({ app, deleteApp }) => (
  <tr key={app.id} className="app-row">
    <td>
      <Link to={`/u/username${thridPartyApps}/${app.id}`} >
        {app.id}
      </Link>
    </td>
    <td>{app.nick}</td>
    <td>{app.prn}</td>
    <td>
      <div className="row justify-content-md-center">
        <div className="btn-group" role="group" aria-label="Device actions">
          <Link className="btn btn-sm btn-light" to={`/u/username${thridPartyApps}/${app.id}`} >
            <i className="mdi mdi-eye-outline"></i>
          </Link>
          <Link className="btn btn-sm btn-info" to={`/u/username${thridPartyAppsEdit}/${app.id}`} >
            <i className="mdi mdi-pencil"></i>
          </Link>
          <ConfirmButton onDelete={() => deleteApp(app.id)} />
        </div>
      </div>
    </td>
  </tr>
)

function AppListRows ({ deleteApp, apps = [], error, status }) {
  if (apps.length === 0 && status === STATES.IN_PROGRESS) {
    return (
      <tr>
        <td colSpan={sortableFields.length + 1}>
          <Loading />
        </td>
      </tr>
    )
  }
  if (error !== null && status === STATES.FAILURE) {
    return (
      <tr>
        <td colSpan={sortableFields.length + 1}>
          <span className="error">
            An error occurred while loading the devices information. Please try
            again later.
          </span>
        </td>
      </tr>
    )
  }

  return (
    <React.Fragment>
      {apps.map((app) => <AppRow key={app.id} app={app} deleteApp={deleteApp} />)}
    </React.Fragment>
  )
}

export default function AppList (props) {
  return (
    <div className="app-list">
      <table className="table table-hover">
        <thead>
          <tr>
            {sortableFields.map(([field, label]) => (
              <th
                key={field}
                onClick={() => props.toggleSorting(field) }
                className={`${sortFieldActive(props.sortField, field)} ${field}`}
              >
                <span>
                  {label}{' '}
                  <i
                    className={`mdi mdi-${sortIconForField(props.sortField, props.sortDirection, field)}`}
                    aria-hidden="true"
                  />
                </span>
              </th>
            ))}
            <th className="text-center actions">Actions</th>
          </tr>
        </thead>
        <tbody>
          <AppListRows {...props} />
        </tbody>
      </table>
    </div>
  )
}
