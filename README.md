# PantaHub

A Web dashboard for Pantacor.

## Run the app on development

In order to run the application on development, install all the dependencies using yarn:

```
yarn
```

then you can start the app with 

```
yarn start
```

### How to run for mobile

Run a live version with capacitor

```
yarn ionic capacitor run android --livereload-url=http://YOUR_PUBLIC_IP:8100 # same command in case of running ios or eletron
```

You need to have a ionic server running in other tab of the console. And for that you do:

```
yarn ionic:serve
```

### Run builded version with capacitor

This will build the application and injected to the native app.

```
yarn ionic capacitor run android
```